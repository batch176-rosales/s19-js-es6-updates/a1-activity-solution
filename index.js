// console.log("GOOD MORNING!");

//#3 & 4
const num=3;
const getCube=num**3;
console.log(`The cube of ${num} is ${getCube}.`);


//#5 & 6
const address=[258,"Logon","Daanbantayan","Cebu"];
const [street,barangay,municipality,city]=address;
console.log(`I live at ${street} ${barangay}, ${municipality}, ${city}.`);


//#7 & 8
const animal= {
    animalName:"lion",
    color:"yellow-gold",
    weight: 130,
    height:4
}
const{animalName,color, weight,height}=animal;

console.log(`A ${animalName} has strong, compact bodies and powerful forelegs, teeth and jaws for pulling down and killing prey. The coats are ${color}. Female ${animalName} weighed at ${weight} kgs with a measurement of ${height} feet tall.`);

//#9 & 10
const number= [1,2,3,4,5];
number.forEach((number) => {console.log (number)})

//#11 & 12
class Dog{
    constructor (name, age, breed){
        this.name=name,
        this.age=age,
        this.breed=breed

    }
}
const myDog = new Dog("Grego",2,"Golden Retriever");
console.log(myDog);
